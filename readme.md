
## Instruction:

1. "vagrant box add laravel/homestead --box-version 0.3.3" (or other versions)
2. "composer install"
3. Configure Homestead.yaml
4. "vagrant up"